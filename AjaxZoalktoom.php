<html>
  <head> 
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>
         <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
         <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" integrity="sha384-LtrjvnR4Twt/qOuYxE721u19sVFLVSA4hf/rRt6PrZTmiPltdZcI7q7PXQBYTKyf" crossorigin="anonymous"></script>
  </head>
  <body>
    <div class="text-center">
         <!-- العنصر رقم 1 -->
         <input type="text" id="value" value=""><br><br><br>

         <!-- العنصر رقم 2 -->
         <button type="button" onclick="loadDoc()" id="button" class="btn btn-dark">ارسال</button>


         <!-- عنصر رقم 3 -->
         <div id="briefinfo">
                     
         </div>
     </div>
    <script>
        // انظر لرقم 2 فيه  
        // loadDoc وبداخله اسم الفنكشن تبعنا onclick حطينا  
        // loadDoc يعني اذا ضغط على البوتون شغل لنا فنكشن تبعنا الي هو 
        function loadDoc() {
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                    if (this.readyState == 4 && this.status == 200) {
                         // contact.php راح نرسل ريكوست ثم بعد ذالك نجيب المحتوى من صفحة 
                         // الي هو رقم ثلاثة 3 briefinfo لما نجيب المحتوى راح نحطه في عنصر 
                         document.getElementById("briefinfo").innerHTML = this.responseText;
                    }
                };
                    xhttp.open("POST", "contact.php", true);
                    xhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    // لاحظ اننا اخذنا القيمة من العنصر رقم 1 الي ادخله المستخدم
                    xhttp.send("name=" + document.getElementById("value").value );
                }
   </script>

  </body>
</html>
